# Users Service 
O **users service** é um sistema desenvolvido para gerenciar acessos, com o objetivo de solidificar conhecimentos em node e algumas bibliotecas.

Ao utilizar esta API, deve ser possível criar um usuário, bem como listar, editar ou excluir tais informações.

Como instalar e rodar? 🚀
Para instalar o sistema, é necessário seguir alguns passos, como baixar o projeto e fazer instalação das dependências. Para isso, é necessário abrir uma aba do terminal e digitar o seguinte:

# Este passo é para baixar o projeto
```cmd
git clone https://gitlab.com/rafael-bertoldo/entrega-app-de-notas.git
```
Depois que terminar de baixar, é necessário entrar na pasta:

# Entrar na pasta
```cmd
cd entrega-app-de-notas
```

# Instalar dependências
```cmd
yarn
```
ou

```cmd
npm install
```

Utilização 🖥️

Faça uma cópia do arquivo ```.env.example``` com o nome ```.env``` e substitua os valores das variáveis para as que gostaria de utilizar:

```js
JWT_SECRET_KEY=your_secret_key // Aqui vai a tua chave secreta
EXPIRES=expires_time // Aqui vai o tempo de expiração do teu token
PORT=execution_port // Aqui vai a porta que utilizará para rodar a API (por padrão utilizamos a porta 3000)
HOST=your_host // Aqui vai o host, se for rodar localmente por padrão utilizamos localhost
```

Depois de configurar seu arquivo ```.env```, é necessário rodar o seguinte comando no terminal para que a API fique online:

```cmd
yarn dev
```

Se tudo der certo a seguinte mensagem aparecerá no console:

```cmd
[nodemon] 2.0.15
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node -r sucrase/register src/index.js`
Runing at http://localhost:3000
```

Para utilizar este sistema, é necessário utilizar um API Client, como o Insomnia

Rotas
GET /users
Esta rota retorna todos os usuários cadastrados no banco.

RESPONSE STATUS -> HTTP 200 (ok)

Header:

```js
{
    Authorization: Bearer token
}
```

Response:

```json
[
	{
		"id": "8a28846d-d956-4a98-9afe-872b449f2831",
		"username": "Monkey D. Luffy",
		"age": 16,
		"email": "mdluffy@mail.com",
		"password": "$2a$10$6.EvUWOpXNdbcgeJPzxcBu3nFPrhraYpxj9X2CuRu98LIsUejA8ki",
		"createdOn": "13/01/2022 19:35:43"
	},
	{
		"id": "93f3d6eb-e7cd-4e3c-b11b-d93d98f369ae",
		"username": "Vimsmoke Sanji",
		"age": 18,
		"email": "sanji@mail.com",
		"password": "$2a$10$PC.izP1z/pYG/SaZPm3QpOm7XHrgUL9iNezH836gYOvK3/Rvpa/9q",
		"createdOn": "13/01/2022 19:35:43"
	}
]
```

POST /signup
Esta rota cadastra um novo usuário no sistema.

Body:

RESPONSE STATUS -> HTTP 201 (Created)

```json
{
	"username": "Vimsmoke Sanji",
	"age": 18,
	"email": "sanji@mail.com",
	"password": "onepiece"
}
```


```json
{
	"id": "93f3d6eb-e7cd-4e3c-b11b-d93d98f369ae",
	"username": "Vimsmoke Sanji",
	"age": 18,
	"email": "sanji@mail.com",
	"createdOn": "13/01/2022 19:35:43"
}
```

POST /login
Esta rota é para adquirir autenticação no sistema.

RESPONSE STATUS -> HTTP 200 (ok)
Body:

```json
{
	"username": "Vimsmoke Sanji",
	"password": "onepiece"
}
```

Response:

```json
{
	"accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ik1vbmtleSBELiBMdWZmeSIsImlhdCI6MTY0MjExMjM3OSwiZXhwIjoxNjQyNzE3MTc5fQ.Vc6-7owKeBbZTvQCPy0eGMBUQ3Rw2mEpH7bViOytgHk"
}
```

PUT /users/&lt;id>/password

Rota para atualizar a senha do usuário.

RESPONSE STATUS -> HTTP 204 (No Content)

Header:

```js
{
    Authorization: Bearer token
}
```

Não há conteúdo no retorno da requisição.


Tecnologias utilizadas 📱

NodeJs

Bcrypt

UUID

JWT

YUP
