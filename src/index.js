import express from "express";
import * as yup from "yup";
import jwt from "jsonwebtoken";
import { v4 as uuidv4 } from "uuid";
import * as bcrypt from "bcryptjs";
import dotenv from "dotenv";

const app = express();
dotenv.config();
const config = {
  secret: process.env.JWT_SECRET_KEY,
  expiresIn: process.env.EXPIRES,
  port: process.env.PORT,
  host: process.env.HOST
};

const date = new Date();

const userSchema = yup.object().shape({
  username: yup.string().required(),
  age: yup.number().required().positive().integer(),
  email: yup.string().required().email(),
  password: yup.string().required(),
  createdOn: yup.date().default(() => date),
});

/*
    MIDDLEWARES
*/

const validate = (schema) => async (req, res, next) => {
  const resource = req.body;
  try {
    await schema.validate(resource);
  } catch (e) {
    console.log(e);
    return res.status(400).json({ error: e.errors.join(", ") });
  }
  next();
};

const authenticateUser = (req, res, next) => {
  let token = req.headers.authorization.split(" ")[1];

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).json({ message: "Invalid Token." });
    }

    let user = USERS.find((usr) => usr.username === decoded.username);

    req.user = user;
  });

  next();
};

const authorizationUser = (req, res, next) => {
  let token = req.headers.authorization.split(" ")[1]

  if (token === undefined) {
    res.status(401).json({message: "please verify you have logged in"})
  }
  next()
}

const updatePassword = async (req, res, next) => {
  const { uuid } = req.params;
  const { password } = req.body;
  

  const { user } = req;

  if (user.id !== uuid) {
    return res
      .status(403)
      .json({ message: "Is not possible change other user password" });
  }

  const hashed = await bcrypt.hash(password, 10)
  console.log(hashed)
  user.password = hashed;

  next();
};

const verificateDuplicateValues = (req, res, next) => {
  const {body} = req

  const user = USERS.find(item => item.email === body.email)

  if (user) {
    return res.status(422).json({message: 'email already exists'})
  }
  next()
}
/*
    MIDDLEWARES
*/

const USERS = [];

app.use(express.json());

/*
    ROTAS
*/

//LIST USERS

app.get("/users", authenticateUser, authorizationUser, (req, res) => {
  res.json(USERS);
});

// CREATE USERS

app.post("/signup", validate(userSchema), verificateDuplicateValues, (req, res) => {
  try {
    const { body } = req;
    bcrypt.hash(body.password, 10).then((hashRes) => {

      const newUser = {
        id: uuidv4(),
        username: body.username,
        age: body.age,
        email: body.email,
        password: hashRes,
        createdOn: date.toLocaleString(),
      };

      USERS.push(newUser);

      const { password: data_password, ...dataWithoutPassword } = newUser;

      return res.status(201).json(dataWithoutPassword);
    });
  } catch (e) {
    res.json({ message: "Error while creating an user" });
  }
});

// LOGIN

app.post("/login", async (req, res) => {
  let { username, password } = req.body;

  const user = await USERS.find(usr => usr.username === username)
  try {
      const match = await bcrypt.compare(password, user.password)

      
      let token = jwt.sign({ username: username }, config.secret, {
        expiresIn: config.expiresIn,
      });
      if(match) {
          res.json({accessToken: token})
      } else {
        res.status(401).json({ message: "Invalid Credentials" })
      }
  } catch (e) {
      console.log(e)
  }

});

// PASSWORD CHANGE

app.put(
  "/users/:uuid/password",
  authenticateUser,
  updatePassword,
  authorizationUser,
  (req, res) => {
    const { user } = req;
    return res.status(204).json(user);
  }
);

/*
    ROTAS
*/

app.listen(config.port, () => {
  console.log(`Runing at http://${config.host}:${config.port}`);
});
